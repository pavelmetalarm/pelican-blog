
FROM python:3.9.5-buster

RUN  apt-get update -y \
    && apt-get upgrade -y && apt-get install -y curl vim \
    && rm -rf /var/lib/apt/lists/* && python -m pip install "pelican[markdown]" 

EXPOSE 8000
WORKDIR /blog
ADD . /blog
ENTRYPOINT ["/bin/bash","entrypoint.sh"]
